# Better Social Share

**Better Social Share** is a Drupal module designed to enhance website social sharing functionality without relying on third-party integrations. It leverages native sharing methods provided by popular social media platforms, offering an efficient and seamless sharing experience for your users. This module supports over 100 social media platforms and provides a comprehensive suite of customization options, enabling you to expand your reach and amplify your content across various online communities.

## Table of Contents

1.  [Requirements](#requirements)
2.  [Recommended Modules](#recommended-modules)
3.  [Installation](#installation)
4.  [Configuration](#configuration)
5.  [Maintainers](#maintainers)

## Requirements

*   Drupal 9.x or later
*   PHP 7.4 or later

## Recommended Modules

*   No extra module is required.

## Installation

*   Install as usual, see [https://www.drupal.org/docs/8/extending-drupal-8/installing-contributed-modules-find-import-enable-configure-drupal-8]( https://www.drupal.org/docs/8/extending-drupal-8/installing-contributed-modules-find-import-enable-configure-drupal-8) for further information.

## Configuration

1.  Navigate to **Admin > Configuration > Web services > Better Social Share**.
2.  Select the social media platforms you'd like to enable for sharing.
3.  Configure the placement of sharing buttons on content types.
4.  Customize the appearance, size, and behavior of the sharing buttons.
5.  Go to the block layout, place the block in the required region, select the 'Better Social Share Buttons' block, and configure the necessary settings to position the block according to your needs, if required.
6.  Save the configuration.

## Maintainers

Current maintainers:

*   Sujan Shrestha - [https://sujanshrestha.com](https://sujanshrestha.com)